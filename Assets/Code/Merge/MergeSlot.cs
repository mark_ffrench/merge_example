﻿using UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class MergeSlot : MonoBehaviour, IDropHandler
{
    public int X;
    public int Y;

    private RectTransform rectTransform;
    public Vector2 Position;
    
    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        Position = rectTransform.anchoredPosition;
    }

    public void OnDrop(PointerEventData eventData)
    {
        GameObject itemObject = eventData.pointerDrag;

        if (itemObject != null)
        {
            MergeItem mergeItem = itemObject.GetComponent<MergeItem>();
            
            if(!mergeItem.IsDraggable())
                return;
            
            mergeItem.SetSlot(this);
        }
    }
}
