﻿using System;

namespace UI
{
    [Serializable]
    public class SlotContents
    {
        public bool empty;
        public bool cobweb;
        public MergeItem.Type type;
        public int level;
    }
}