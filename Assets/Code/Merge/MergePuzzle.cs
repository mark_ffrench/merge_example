﻿using System;

namespace UI
{
    public class MergePuzzle : MergeItem
    {
        private bool IsLargeEnoughToPlay()
        {
            return level >= MinPlayableLevel;
        }

        public bool IsPlayable()
        {
            return IsUnlocked() && IsLargeEnoughToPlay();
        }

        protected override void Refresh()
        {
            base.Refresh();

            bool playable = IsPlayable();
            ToggleShine(playable);
            TogglePulse(playable);
            
            int puzzleSize = Level + 1;
            label.text = $"{puzzleSize}x{puzzleSize}";
            
            spriteImage.color = GetColor(itemType);
        }
        
        public TrailStage GetStage()
        {
            switch (ItemType)
            {
                case Type.Bakery:
                    return TrailStage.Baking;
                case Type.Cleaning:
                    return TrailStage.Cleaning;
                case Type.Gardening:
                    return TrailStage.Gardening;
                case Type.Fishing:
                    return TrailStage.Fishing;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}