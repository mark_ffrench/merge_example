﻿using System;
using Helpers;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    //this is like a chest but it has a cooldown instead of getting destroyed when it's empty
    public class MergeSpawner : MergeItem
    {
        public int NumDrops { get; private set; }
        public DateTime RechargeAt { get; private set; }
        
        private int RechargeTimeInSeconds = 30;
        private DropTable<(Type, int)> dropTable;

        [SerializeField] private Image progressBar;
        
        protected override void Refresh()
        {
            base.Refresh();
            label.text = NumDrops.ToString();
        }

        public void FinishSetup()
        {
            dropTable = GenerateDropTable(itemType);
            NumDrops = GetMaxDrops(level);
            ToggleShine(true);
        }
        
        public (Type, int level) ClaimRandomDrop()
        {
            NumDrops--;
            Refresh();

            if (NumDrops <= 0)
            {
                RechargeAt = DateTimeProvider.Now.AddSeconds(RechargeTimeInSeconds);
                ToggleShine(false);
            }
            
            return dropTable.PickRandom();
        }

        public void Update()
        {
            double remainingSeconds = (RechargeAt - DateTimeProvider.Now).TotalSeconds;
            float t = (float)remainingSeconds / RechargeTimeInSeconds;
            
            progressBar.fillAmount = 1f - t;

            if (remainingSeconds < 0)
            {
                NumDrops = GetMaxDrops(level);
                ToggleShine(true);
            }
        }

        protected virtual int GetMaxDrops(int chestLevel)
        {
            switch (chestLevel)
            {
                case 2:
                    return 1;
                case 3:
                    return 2;
                case 4:
                    return 4;
                case 5:
                    return 8;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        

        protected virtual DropTable<(Type, int)> GenerateDropTable(Type type)
        {
            DropTable<(Type, int)> dropTable = new DropTable<(Type, int)>();
            
            if (type == Type.Bakery)
            {
                dropTable.WithChanceOf((Type.Cleaning, 1), 0.15f);
                dropTable.WithChanceOf((Type.Cleaning, 2), 0.3f);
                dropTable.OrElse((Type.Cleaning, 0));
            }

            if (type == Type.Cleaning)
            {
                dropTable.WithChanceOf((Type.Gardening, 1), 0.15f);
                dropTable.WithChanceOf((Type.Gardening, 2), 0.3f);
                dropTable.OrElse((Type.Gardening, 0));
            }

            if (type == Type.Gardening)
            {
                dropTable.WithChanceOf((Type.Fishing, 1), 0.15f);
                dropTable.WithChanceOf((Type.Fishing, 2), 0.3f);
                dropTable.OrElse((Type.Fishing, 0));
            }

            if (type == Type.Fishing)
            {
                dropTable.WithChanceOf((Type.Bakery, 1), 0.15f);
                dropTable.WithChanceOf((Type.Bakery, 2), 0.3f);
                dropTable.OrElse((Type.Bakery, 0));
            }

            return dropTable;
        }
    }
}