﻿using Merge;
using UI;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MergePuzzleConfig))]
public class MergePuzzleConfigInspector : Editor
{
    private SerializedProperty boardConfigProperty;
    private SerializedProperty contentsArray;

    private int selectedIndex = 0;
    
    private void OnEnable()
    {
        boardConfigProperty = serializedObject.FindProperty("startBoardConfig");
        contentsArray = boardConfigProperty.FindPropertyRelative("Contents");
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        int width = MergePuzzleBoardConfig.width;
        int height = MergePuzzleBoardConfig.height;

        SerializedProperty selectedSlot = contentsArray.GetArrayElementAtIndex(selectedIndex);
        // MergeItem.Type selectedType = (MergeItem.Type) selectedSlot.FindPropertyRelative("type").intValue;
        // int selectedLevel = selectedSlot.FindPropertyRelative("level").intValue;

        SerializedProperty emptyProp = selectedSlot.FindPropertyRelative("empty");

        EditorGUILayout.PropertyField(selectedSlot.FindPropertyRelative("empty"), new GUIContent("Empty"));
        
        if (!emptyProp.boolValue)
        {
            EditorGUILayout.PropertyField(selectedSlot.FindPropertyRelative("cobweb"), new GUIContent("Cobweb"));
            EditorGUILayout.PropertyField(selectedSlot.FindPropertyRelative("type"), new GUIContent("Type"));
            EditorGUILayout.PropertyField(selectedSlot.FindPropertyRelative("level"), new GUIContent("Lvl"));
        }

        serializedObject.ApplyModifiedProperties();
        
        EditorGUILayout.BeginVertical();
        for (int y = 0; y < height; y++)
        {
            EditorGUILayout.BeginHorizontal();

            for (int x = 0; x < width; x++)
            {
                int index = y * width + x;

                string label = "";
                GUIStyle style = GUI.skin.button;

                SerializedProperty prop = contentsArray.GetArrayElementAtIndex(index);
                bool empty = prop.FindPropertyRelative("empty").boolValue;

                if (!empty)
                {
                    bool cobweb = prop.FindPropertyRelative("cobweb").boolValue;

                    MergeItem.Type type = (MergeItem.Type) prop.FindPropertyRelative("type").intValue;
                    int level = prop.FindPropertyRelative("level").intValue;

                    style.normal.textColor = MergeItem.GetColor(type);
                    
                    string typeString = type.ToString().Replace("Puzzle", "");

                    if (cobweb)
                    {
                        label = $"#{typeString}:{level}#";
                    }
                    else
                    {
                        label = $"{typeString}:{level}";
                    }

                }

                GUIContent content = new GUIContent(label);
                
                if (GUILayout.Button(content, GUI.skin.button, GUILayout.Width(100)))
                {
                    selectedIndex = index;
                }
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
    }
}
