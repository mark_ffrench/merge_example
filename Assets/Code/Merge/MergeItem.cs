﻿using System;
using Helpers;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    //ideas for MergeItem types
    
    //Epic puzzles & clues - A large puzzle which has missing clues.
    //You must collect clues by solving other smaller puzzles. You can revisit the puzzle whenever you
    //find a new clue and spend some time partially solving it. The puzzle gives out lots of valuable rewards for every
    //step you take solving it, so it's always worth checking to see what the new clue has revealed. You can pay/watch
    //an ad to gain an extra clue (but you can only do this once or twice per day).
    
    //Timed puzzles - A puzzle with a strict time limit. If you don't solve the puzzle in time, the puzzle locks for
    //X hours, after which you can try again. If you run out of time, you can pay/watch an ad to get a time extension

    //Sudden death - A puzzle where you only have one life. Making a mistake locks the puzzle for X hours. You can pay
    //to skip the cooldown, or watch an ad to shave off 30 mins.
    
    //Merged theme puzzles - For example, merge gardening and cleaning puzzles to get discover the farming puzzle.
    //Similar to Alchemy games.
    
    //Crafting items - A conventional mergeable item that can be used to complete crafting recipes.
    
    //XP, Coins, Gems, Energy
    
    //Minigames - Scratchcard, crossword etc.
    
    //Spawners - Chests with a cooldown
    
    //Piggy banks/Energy Chests
    
    public class MergeItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, 
        IEndDragHandler, IDragHandler, IDropHandler, IPointerClickHandler
    {
        [SerializeField] private Sprite cobwebSprite;
        [SerializeField] private Sprite fogSprite;
        [SerializeField] private Sprite chestSprite;
        [SerializeField] private Sprite puzzleSprite;
        [SerializeField] private CanvasGroup canvasgroup;
        [SerializeField] protected TextMeshProUGUI label;
        [SerializeField] private GameObject selectionHighlight;
        [SerializeField] private Image overlayImage;
        [SerializeField] protected Image spriteImage;
        [SerializeField] protected GameObject starBurst;
        
        private float animTimer = 0f;
        private const float AnimDuration = 0.3f;
        private MergeSlot destinationSlot;
        private bool embiggened = false;
        private UIShineEffect shineEffect;
        private Canvas canvas;
        private PuzzleMergePrototypeScreen parentScreen;
        
        protected bool visible = true;
        protected bool cobwebbed = false;
        protected Type itemType;
        protected int level;
        protected Image bgdImage;
        protected RectTransform rectTransform;

        public MergeSlot CurrentSlot { get; private set; }
        public bool IsAnimating { get; private set; }
        
        public const int MaxLevel = 5;
        public const int MinPlayableLevel = 2;
        
        public void AnimateToSlot(MergeSlot slot)
        {
            if (IsAnimating)
                StopAllCoroutines();

            animTimer = 0f;
            IsAnimating = true;
            destinationSlot = slot;
            Ease.EaseTarget(0.5f, Easing.Exponential.Out, 0.1f, AnimateToDestinationSlot, OnFinishedAnim);
        }

        private void OnFinishedAnim()
        {
            IsAnimating = false;
            SetSlot(destinationSlot);
            destinationSlot = null;
        }

        private void AnimateToDestinationSlot(float t)
        {
            rectTransform.anchoredPosition = Vector2.Lerp(CurrentSlot.Position, destinationSlot.Position, t);
        }

        public bool Cobwebbed
        {
            get => cobwebbed;
            set
            {
                cobwebbed = value;
                Refresh();
            }
        }

        public bool Visible
        {
            get => visible;
            set
            {
                visible = value;
                Refresh();
            }
        }

        protected void TogglePulse(bool enabled)
        {
            if (pulseCoroutine != null == enabled)
            {
                return;
            }

            if (enabled)
            {
                StartPulse();
            }
            else
            {
                StopPulse();
            }
        }

        public bool IsDraggable()
        {
            return visible && !cobwebbed && !IsAnimating;
        }

        protected virtual void Refresh()
        {
            overlayImage.gameObject.SetActive(!IsUnlocked());
            
            if (!visible)
            {
                overlayImage.sprite = fogSprite;
            }
            else if (cobwebbed)
            {
                overlayImage.sprite = cobwebSprite;
            }
        }

        protected bool IsUnlocked()
        {
            return visible && !cobwebbed;
        }

        public int Level
        {
            get => level;
            set
            {
                level = value;
                Refresh();
            }
        }

        public Type ItemType
        {
            get => itemType;
            set
            {
                itemType = value;
                Refresh();
            }
        }

        public static Color GetColor(Type itemType)
        {
            switch (itemType)
            {
                case Type.Bakery:
                    return Color.yellow;
                case Type.Cleaning:
                    return Color.cyan;
                case Type.Gardening:
                    return Color.green;
                case Type.Fishing:
                    return Color.blue;
                default:
                    throw new ArgumentOutOfRangeException(nameof(itemType), itemType, null);
            }
        }

        public enum Type
        {
            Bakery,
            Cleaning,
            Gardening,
            Fishing
        }
        
        private void Awake()
        {
            bgdImage = GetComponent<Image>();
            canvasgroup = GetComponent<CanvasGroup>();
            rectTransform = GetComponent<RectTransform>();
            shineEffect = GetComponent<UIShineEffect>();
            starBurst.SetActive(false);
        }

        protected void ToggleShine(bool enabled)
        {
            if (shineEffect != null)
            {
                shineEffect.enabled = enabled;
            }
        }

        private void Start()
        {
            Refresh();
        }

        private Vector3 EmbiggenedScale = new Vector3(1.5f, 1.5f, 1.5f);
        private Vector3 PulseScale = new Vector3(1.25f, 1.25f, 1.25f);

        public void OnPointerEnter(PointerEventData eventData)
        {
            GameObject draggedObject = eventData.pointerDrag;

            if (draggedObject != null)
            {
                MergeItem mergeItem = draggedObject.GetComponent<MergeItem>();

                if (!(mergeItem is null) && CanMerge(this, mergeItem))
                {
                    Embiggen();
                }
            }
        }
        
        private void Embiggen()
        {
            embiggened = true;
            Ease.EaseTarget(0.4f, Easing.Quadratic.Out, 0f,
                (float t) => { rectTransform.localScale = Vector3.Lerp(Vector3.one, EmbiggenedScale, t); },
                () => { rectTransform.localScale = EmbiggenedScale; });
            
            starBurst.SetActive(true);
        }

        private void Unbiggen()
        {
            embiggened = false;
            Ease.EaseTarget(0.25f, Easing.Exponential.In, 0f,
                (float t) => { rectTransform.localScale = Vector3.Lerp(EmbiggenedScale, Vector3.one, t); },
                () => { rectTransform.localScale = Vector3.one; });
            
            starBurst.SetActive(false);
        }

        private Coroutine pulseCoroutine;

        private void StartPulse()
        {
            if (pulseCoroutine != null)
            {
                StopCoroutine(pulseCoroutine);
            }
            
            pulseCoroutine = Ease.PulseTarget(1f, Easing.Quadratic.In, Easing.Quadratic.Out, 3f, UpdatePulse, OnFinishPulse);
        }

        private void StopPulse()
        {
            if (pulseCoroutine != null)
            {
                StopCoroutine(pulseCoroutine);
                pulseCoroutine = null;
                UpdatePulse(0f);
            }
        }
        
        private void OnFinishPulse()
        {
            StartPulse();
        }
        
        private void UpdatePulse(float t)
        {
            rectTransform.localScale = Vector3.Lerp(Vector3.one, PulseScale, t);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            spriteImage.rectTransform.localScale = new Vector3(1f, 1f, 1f);
            
            if(embiggened)
                Unbiggen();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (!IsDraggable())
                return;
            
            StopPulse();
            canvasgroup.blocksRaycasts = false;
            parentScreen.SelectItem(null);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if(!IsDraggable())
                return;
            
            Refresh();
            canvasgroup.blocksRaycasts = true;
            rectTransform.anchoredPosition = CurrentSlot.Position;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!IsDraggable())
                return;
            
            rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
        }

        public void SetSlot(MergeSlot slot)
        {
            Debug.Log($"Item is now in {slot.X},{slot.Y}");
            CurrentSlot = slot;
        }

        public void Setup(MergeSlot slot, PuzzleMergePrototypeScreen parentScreen, Canvas parentCanvas)
        {
            if (rectTransform == null)
            {
                Awake();
            }
            
            CurrentSlot = slot;
            rectTransform.anchoredPosition = CurrentSlot.Position;
            this.parentScreen = parentScreen;
            canvas = parentCanvas;
        }
        
        public void Upgrade()
        {
            Level++;
        }

        public void OnDrop(PointerEventData eventData)
        {
            canvasgroup.alpha = 1f;
            spriteImage.rectTransform.localScale = new Vector3(1f, 1f, 1f);
            
            GameObject itemObject = eventData.pointerDrag;

            if (itemObject != null)
            {
                MergeItem mergeItem = itemObject.GetComponent<MergeItem>();

                if (mergeItem == null)
                {
                    Debug.LogError("WTF is this you just dropped on me?? "+itemObject);
                    return;
                }
                
                if(!mergeItem.IsDraggable())
                    return;
                
                parentScreen.Merge(this, mergeItem);

                if (embiggened)
                {
                    Unbiggen();
                }
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (IsAnimating)
                return;
            
            parentScreen.SelectItem(this);
            selectionHighlight.SetActive(true);
        }

        public void Deselect()
        {
            selectionHighlight.SetActive(false);
        }

        public static bool CanMerge(MergeItem itemInSlot, MergeItem droppedItem)
        {
            if (!droppedItem.IsDraggable())
            {
                return false;
            }

            if (!itemInSlot.Visible)
            {
                return false;
            }

            if (itemInSlot.ItemType != droppedItem.ItemType)
            {
                return false;
            }

            if (itemInSlot.Level != droppedItem.Level)
            {
                return false;
            }

            if (itemInSlot.Level == MaxLevel)
            {
                return false;
            }

            return true;
        }
    }
}