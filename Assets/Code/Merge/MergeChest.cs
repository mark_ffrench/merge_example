﻿using System;
using Helpers;

namespace UI
{
    public class MergeChest : MergeItem
    {
        public int NumDrops { get; private set; }
        private DropTable<(Type, int)> dropTable;
        
        protected override void Refresh()
        {
            base.Refresh();
            label.text = NumDrops.ToString();
        }

        public void FinishSetup()
        {
            dropTable = GenerateDropTable(itemType);
            NumDrops = GetMaxDrops(level);
            ToggleShine(true);
        }
        
        public (Type, int level) ClaimRandomDrop()
        {
            NumDrops--;
            Refresh();

            return dropTable.PickRandom();
        }
        
        protected virtual int GetMaxDrops(int chestLevel)
        {
            switch (chestLevel)
            {
                case 2:
                    return 1;
                case 3:
                    return 2;
                case 4:
                    return 4;
                case 5:
                    return 8;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        

        protected virtual DropTable<(Type, int)> GenerateDropTable(Type type)
        {
            DropTable<(Type, int)> dropTable = new DropTable<(Type, int)>();
            
            if (type == Type.Bakery)
            {
                dropTable.WithChanceOf((Type.Cleaning, 1), 0.15f);
                dropTable.WithChanceOf((Type.Cleaning, 2), 0.3f);
                dropTable.OrElse((Type.Cleaning, 0));
            }

            if (type == Type.Cleaning)
            {
                dropTable.WithChanceOf((Type.Gardening, 1), 0.15f);
                dropTable.WithChanceOf((Type.Gardening, 2), 0.3f);
                dropTable.OrElse((Type.Gardening, 0));
            }

            if (type == Type.Gardening)
            {
                dropTable.WithChanceOf((Type.Fishing, 1), 0.15f);
                dropTable.WithChanceOf((Type.Fishing, 2), 0.3f);
                dropTable.OrElse((Type.Fishing, 0));
            }

            if (type == Type.Fishing)
            {
                dropTable.WithChanceOf((Type.Bakery, 1), 0.15f);
                dropTable.WithChanceOf((Type.Bakery, 2), 0.3f);
                dropTable.OrElse((Type.Bakery, 0));
            }

            return dropTable;
        }
    }
}