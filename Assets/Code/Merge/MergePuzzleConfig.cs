﻿using System;
using Helpers;
using UI;
using UnityEngine;

namespace Merge
{
    [CreateAssetMenu]
    public class MergePuzzleConfig : ScriptableObject
    {
        public MergePuzzleBoardConfig startBoardConfig;
    }

    [Serializable]
    public class MergePuzzleBoardConfig
    {
        public const int width = 6;
        public const int height = 5;
        
        public SlotContents[] Contents = new SlotContents[width * height];

        public SlotContents Get(int x, int y)
        {
            int index = y * width + x;
            if (!Contents.IsValidIndex(index))
            {
                throw new ArgumentOutOfRangeException($"{x},{y} out of range");
            }
            return Contents[index];
        }
    }
}