﻿using System;
using System.Collections.Generic;
using System.Linq;
using Helpers;
using Levels;
using Merge;
using TMPro;
using UnityEngine;

namespace UI
{
    public class PuzzleMergePrototypeScreen : UIScreen
    {
        private MergeSlot[] slots;
        [SerializeField] private MergePuzzle puzzleItemPrefab;
        [SerializeField] private MergeChest chestItemPrefab;
        [SerializeField] private MergeSpawner spawnerPrefab;
        [SerializeField] private RectTransform container;

        [SerializeField] private TextMeshProUGUI descriptionLabel;
        [SerializeField] private MergePuzzleConfig startConfig;

        private List<MergeItem> items = new List<MergeItem>();

        private MergeItem selectedItem;

        [Inject] private ThemeCollection themeCollection;
        [Inject] private PuzzleController puzzleController;

        [SerializeField] private AudioSource mergeChimeAudio;
        [SerializeField] private AudioClip[] mergeChimeAudioClips;
        
        protected override void OnShow()
        {
            base.OnShow();

            if (slots == null)
            {
                slots = GetComponentsInChildren<MergeSlot>();

                InitialSpawn();
            }
        }

        private void InitialSpawn()
        {
            foreach (MergeSlot slot in slots)
            {
                SlotContents contents = startConfig.startBoardConfig.Get(slot.X, slot.Y);

                if (!contents.empty)
                {
                    MergeItem item = SpawnPuzzle(slot, contents.type, contents.level);
                    item.Cobwebbed = contents.cobweb;

                    item.Visible = !item.Cobwebbed;
                }
            }

            //reveal neighbours after spawning to make sure the items are all there to reveal
            foreach (MergeSlot slot in slots)
            {
                SlotContents contents = startConfig.startBoardConfig.Get(slot.X, slot.Y);
                if (contents.empty || !contents.cobweb)
                {
                    RevealNeighbours(slot);
                }
            }
        }

        private MergePuzzle SpawnPuzzle(MergeSlot spawnSlot, MergeItem.Type type, int level)
        {
            if (spawnSlot == null)
            {
                throw new ArgumentNullException(nameof(spawnSlot));
            }

            MergePuzzle item = Instantiate(puzzleItemPrefab, container);
            item.Setup(spawnSlot, this, canvas);
            item.ItemType = type;
            item.Level = level;
            items.Add(item);
            return item;
        }

        private MergeChest SpawnChest(MergeSlot spawnSlot, MergeItem.Type type, int level)
        {
            if (spawnSlot == null)
            {
                throw new ArgumentNullException(nameof(spawnSlot));
            }

            MergeChest item = Instantiate(chestItemPrefab, container);
            item.Setup(spawnSlot, this, canvas);
            item.ItemType = type;
            item.Level = level;
            item.FinishSetup();
            items.Add(item);
            return item;
        }
        
        private MergeSpawner SpawnSpawner(MergeSlot spawnSlot, MergeItem.Type type, int level)
        {
            if (spawnSlot == null)
            {
                throw new ArgumentNullException(nameof(spawnSlot));
            }

            MergeSpawner item = Instantiate(spawnerPrefab, container);
            item.Setup(spawnSlot, this, canvas);
            item.ItemType = type;
            item.Level = level;
            item.FinishSetup();
            items.Add(item);
            return item;
        }

        public void DebugSpawnPuzzle()
        {
            MergeSlot[] emptySlots = EmptySlots();

            if (emptySlots.Length > 0)
            {
                MergeSlot spawnSlot = emptySlots.PickRandom();
                SpawnPuzzle(spawnSlot, MergeItem.Type.Bakery, 0);
            }
        }
        
        public void DebugSpawnChest()
        {
            MergeSlot[] emptySlots = EmptySlots();

            if (emptySlots.Length > 0)
            {
                MergeSlot spawnSlot = emptySlots.PickRandom();
                SpawnChest(spawnSlot, MergeItem.Type.Bakery, 4);
            }
        }
        
        public void DebugSpawnSpawner()
        {
            MergeSlot[] emptySlots = EmptySlots();

            if (emptySlots.Length > 0)
            {
                MergeSlot spawnSlot = emptySlots.PickRandom();
                SpawnSpawner(spawnSlot, MergeItem.Type.Bakery, 4);
            }
        }

        private MergeSlot[] EmptySlots()
        {
            return slots.Except(items.Select(i => i.CurrentSlot)).ToArray();
        }

        /// <summary>
        /// returned in clockwise order from the top (top, right, bottom, left)
        /// </summary>
        private IEnumerable<MergeSlot> GetOrthogonalNeighbours(MergeSlot centerSlot)
        {
            if (centerSlot.Y > 0)
            {
                yield return GetSlot(centerSlot.X, centerSlot.Y - 1);
            }

            if (centerSlot.X < MergePuzzleBoardConfig.width - 1)
            {
                yield return GetSlot(centerSlot.X + 1, centerSlot.Y);
            }

            if (centerSlot.Y < MergePuzzleBoardConfig.height - 1)
            {
                yield return GetSlot(centerSlot.X, centerSlot.Y + 1);
            }

            if (centerSlot.X > 0)
            {
                yield return GetSlot(centerSlot.X - 1, centerSlot.Y);
            }
        }

        private MergeSlot GetSlot(int x, int y)
        {
            int index = y * MergePuzzleBoardConfig.width + x;

            if (x < 0 || y < 0 || x >= MergePuzzleBoardConfig.width || y >= MergePuzzleBoardConfig.height ||
                !slots.IsValidIndex(index))
            {
                throw new ArgumentOutOfRangeException($"Unknown board coord {x},{y}");
            }

            return slots[index];
        }

        private MergeItem GetItemInSlot(int x, int y)
        {
            return items.FirstOrDefault(mergeItem => mergeItem.CurrentSlot.X == x && mergeItem.CurrentSlot.Y == y);
        }

        public void Merge(MergeItem itemInSlot, MergeItem droppedItem)
        {
            if (MergeItem.CanMerge(itemInSlot, droppedItem))
            {
                items.Remove(droppedItem);
                Destroy(droppedItem.gameObject);
                itemInSlot.Upgrade();

                if (itemInSlot.Cobwebbed)
                {
                    itemInSlot.Cobwebbed = false;

                    MergeSlot slot = itemInSlot.CurrentSlot;
                    RevealNeighbours(slot);
                }

                AudioClip clip = mergeChimeAudioClips[itemInSlot.Level];
                mergeChimeAudio.PlayOneShot(clip);
            }
        }

        private void RevealNeighbours(MergeSlot slot)
        {
            foreach (MergeSlot neighbourSlot in GetOrthogonalNeighbours(slot))
            {
                MergeItem item = GetItemInSlot(neighbourSlot.X, neighbourSlot.Y);
                if (item != null)
                {
                    item.Visible = true;
                }
            }
        }

        public void SelectItem(MergeItem mergeItem)
        {
            if (mergeItem != null && selectedItem == mergeItem)
            {
                PlaySelected();
                return;
            }
            
            if (selectedItem != null)
            {
                selectedItem.Deselect();
            }

            selectedItem = mergeItem;

            if (selectedItem != null)
            {
                descriptionLabel.gameObject.SetActive(true);
                descriptionLabel.text = "";

                if (!selectedItem.Visible)
                {
                    descriptionLabel.text = "???\nThis item has not been revealed yet.";
                }
                else
                {
                    if (selectedItem is MergePuzzle puzzle)
                    {
                        descriptionLabel.text = "Puzzle";
                    }
                    else if (selectedItem is MergeChest chest)
                    {
                        descriptionLabel.text = "Chest";
                    }
                    else if (selectedItem is MergeSpawner spawner)
                    {
                        descriptionLabel.text = "Spawner";
                    }

                    descriptionLabel.text += " Level " + (selectedItem.Level + 1);

                    if (selectedItem.Level == MergeItem.MaxLevel)
                    {
                        descriptionLabel.text += " MAX\n" +
                                                 "This item can't be merged any further.";
                    }
                }
            }
            else
            {
                descriptionLabel.gameObject.SetActive(false);
            }
        }

        public void PlaySelected()
        {
            if (selectedItem == null)
            {
                Debug.LogError("No item selected");
                return;
            }

            if (selectedItem is MergePuzzle puzzle)
            {
                if (!puzzle.IsPlayable())
                {
                    return;
                }
                
                TrailStage stage = puzzle.GetStage();

                PuzzleBoard.Data data = new PuzzleBoard.Data
                {
                    Theme = themeCollection.GetTheme(stage, 0)
                };

                PuzzleTheme.Randomise();

                MergePuzzleTracker tracker = new MergePuzzleTracker();
                tracker.OnPuzzleComplete += OnPuzzleComplete;
                puzzleController.CompletionTracker = tracker;

                GoTo<PuzzleBoard>(data);

                int width = puzzle.Level + 1;
                int height = width;

                puzzleController.StartCustomPuzzle(width, height, 1, ClueGenerator.MediumClues);
            }
            else if (selectedItem is MergeChest chest)
            {
                MergeSlot[] emptySlots = EmptySlots();

                if (emptySlots.Length > 0)
                {
                    var spawnSlot = emptySlots.PickRandom();
                    (MergeItem.Type dropType, int dropLevel) = chest.ClaimRandomDrop();
                    
                    MergePuzzle drop = SpawnPuzzle(selectedItem.CurrentSlot, dropType, dropLevel);
                    drop.AnimateToSlot(spawnSlot);
                    
                    if (chest.NumDrops <= 0)
                    {
                        RemoveItem(chest);
                    }
                }
            }
            else if (selectedItem is MergeSpawner spawner)
            {
                if(spawner.NumDrops <= 0)
                    return;
                
                MergeSlot[] emptySlots = EmptySlots();

                if (emptySlots.Length > 0 )
                {
                    var spawnSlot = emptySlots.PickRandom();
                    (MergeItem.Type dropType, int dropLevel) = spawner.ClaimRandomDrop();
                    
                    MergePuzzle drop = SpawnPuzzle(selectedItem.CurrentSlot, dropType, dropLevel);
                    drop.AnimateToSlot(spawnSlot);
                }
            }
        }

        private void OnPuzzleComplete()
        {
            MergeItem.Type spawnerType = selectedItem.ItemType;
            int spawnerLevel = selectedItem.Level;

            MergeSlot slot = selectedItem.CurrentSlot;
            RemoveItem(selectedItem);
            SpawnChest(slot, spawnerType, spawnerLevel);

            selectedItem = null;
        }

        private void RemoveItem(MergeItem item)
        {
            items.Remove(item);
            Destroy(item.gameObject);
        }

        public void Back()
        {
            GoTo<DebugPanel>();
        }
    }
}